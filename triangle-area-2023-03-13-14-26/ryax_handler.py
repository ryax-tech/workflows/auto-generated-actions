def handle(mod_in):
    base = mod_in.get("base")
    height = mod_in.get("height")
    area = 0.5 * base * height
    return {"area": area}
