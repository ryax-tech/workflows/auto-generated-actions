import tweepy

def handle(mod_in):
    company_name = mod_in.get("company_name")
    consumer_key = mod_in.get("consumer_key")
    consumer_secret = mod_in.get("consumer_secret")
    access_token = mod_in.get("access_token")
    access_token_secret = mod_in.get("access_token_secret")

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    api = tweepy.API(auth)

    try:
        user = api.get_user(company_name)
        return {"twitter_handle": user.screen_name}
    except tweepy.TweepError as e:
        return {"error": str(e)}
