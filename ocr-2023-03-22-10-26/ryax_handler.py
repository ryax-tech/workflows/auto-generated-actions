import pytesseract
from PIL import Image


def handle(mod_in):
    img_path = mod_in.get("image_path")
    lang = mod_in.get("language")
    img = Image.open(img_path)
    text = pytesseract.image_to_string(img, lang=lang)
    return {"text": text}
