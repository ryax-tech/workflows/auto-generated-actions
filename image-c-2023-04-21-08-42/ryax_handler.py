import os
import json
import requests
from PIL import Image
import torch
import torchvision.transforms as transforms

def handle(mod_in):
    img_path = mod_in.get("image")
    model_url = mod_in.get("model_url")
    device = mod_in.get("device")
    transform = transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406],
                             std=[0.229, 0.224, 0.225])
    ])
    img = Image.open(img_path)
    img_tensor = transform(img)
    img_tensor = img_tensor.unsqueeze(0)
    headers = {'Content-type': 'application/json'}
    data = json.dumps({"inputs": img_tensor.tolist()})
    response = requests.post(model_url, headers=headers, data=data)
    output = json.loads(response.text)
    _, preds = torch.max(torch.tensor(output['outputs']), 1)
    labels_url = mod_in.get("labels_url")
    labels = requests.get(labels_url).text.split('\n')
    items = [labels[pred] for pred in preds]
    return {"items": items}

