import requests
import os

def handle(mod_in):
    url = mod_in.get("url")
    filename = mod_in.get("filename")
    response = requests.get(url)
    with open(f"/tmp/{filename}", "wb") as f:
        f.write(response.content)
    return {"file": f"/tmp/{filename}"}
