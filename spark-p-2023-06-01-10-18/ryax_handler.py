from pyspark.sql import SparkSession

def handle(mod_in):
    spark = SparkSession.builder.appName("Pi").getOrCreate()
    partitions = mod_in.get("partitions")
    n = 100000 * partitions

    def f(_):
        import random
        x = random.random() * 2 - 1
        y = random.random() * 2 - 1
        return 1 if x ** 2 + y ** 2 <= 1 else 0

    count = spark.sparkContext.parallelize(range(1, n + 1), partitions).map(f).reduce(lambda x, y: x + y)
    pi = 4.0 * count / n
    spark.stop()
    return {"pi": pi}
