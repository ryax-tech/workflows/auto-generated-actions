import requests

def handle(mod_in):
    prompt = mod_in.get("prompt")
    response = requests.post(
        "https://api.openai.com/v1/engines/davinci-codex/completions",
        headers={
            "Content-Type": "application/json",
            "Authorization": f"Bearer {mod_in.get('api_key')}"
        },
        json={
            "prompt": prompt,
            "max_tokens": mod_in.get("max_tokens"),
            "temperature": mod_in.get("temperature"),
            "n": mod_in.get("n"),
            "stop": mod_in.get("stop")
        }
    )
    return {"response": response.json()["choices"][0]["text"]}
