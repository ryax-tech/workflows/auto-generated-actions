import wikipediaapi
import json

def handle(mod_in):
    keyword = mod_in.get("keyword")
    wiki = wikipediaapi.Wikipedia('en')
    page = wiki.page(keyword)
    title = page.title
    summary = page.summary
    links = [link.title for link in page.links.values()]

    data = {"summary": summary, "links": links}
    filename = f"/tmp/{title}.json"
    with open(filename, "w") as f:
        json.dump(data, f)

    return {"filename": filename}
