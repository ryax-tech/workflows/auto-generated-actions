import os
import pandas as pd
import yfinance as yf

def handle(mod_in):
    start_date = mod_in.get("start_date")
    end_date = mod_in.get("end_date")
    output_dir = mod_in.get("output_dir")
    
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    
    cac40_tickers = pd.read_csv('/tmp/cac40_tickers.csv')['Ticker'].tolist()
    
    for ticker in cac40_tickers:
        stock = yf.Ticker(ticker)
        df = stock.history(start=start_date, end=end_date)
        df.to_csv(os.path.join(output_dir, f"{ticker}.csv"), index=True)
    
    return {"stdout": f"Successfully saved daily close prices for CAC40 stocks from {start_date} to {end_date} in {output_dir}"}
