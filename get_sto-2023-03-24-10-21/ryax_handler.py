import yfinance as yf

def handle(mod_in):
    ticker = mod_in.get("ticker")
    period = mod_in.get("period")
    interval = mod_in.get("interval")
    stock_data = yf.download(ticker, period=period, interval=interval)
    stock_data.to_csv("/tmp/stock_data.csv")
    return {"stdout": "Stock data downloaded and saved to /tmp/stock_data.csv"}
