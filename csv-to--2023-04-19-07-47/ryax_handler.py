import csv
import os
from airtable import Airtable

def handle(mod_in):
    csv_file = mod_in.get("csv_file")
    airtable_api_key = mod_in.get("airtable_api_key")
    airtable_base_key = mod_in.get("airtable_base_key")
    airtable_table_name = mod_in.get("airtable_table_name")

    # Read CSV file
    with open(csv_file, "r") as f:
        reader = csv.DictReader(f)
        rows = [row for row in reader]

    # Write to Airtable
    airtable = Airtable(airtable_base_key, airtable_table_name, api_key=airtable_api_key)
    for row in rows:
        airtable.insert(row)

    return {"message": "CSV data successfully written to Airtable."}
